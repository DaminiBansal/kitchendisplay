var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var addProduct = new Schema({
    dishname: { type: String, required: true, unique: true },
    quantity: { type: Number },
    prediction: { type: Number },
    createdtillnow: { type: Number }
});

addProduct.methods.toJSON = function() {
    var obj = this.toObject();
    delete obj.__v;
    delete obj._id;
    return obj;

}



module.exports = mongoose.model('addProduct', addProduct);