var express = require('express');
var app = express();
var router = express.Router();
var path = require('path');


app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');


/* GET home page. */
router.get('/', function(req, res, next) {
    //res.render('index', { title: 'Express' });
    res.sendFile(path.resolve('routes/kitchendisplay.html'));
});



module.exports = router;