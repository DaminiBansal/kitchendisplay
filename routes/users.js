var express = require('express');
var router = express.Router();
var app = express();
var addProduct = require('./../model/addProduct');
var Promise = require('bluebird');
var bodyParser = require('body-parser');

/* GET users listing. */
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const excel = require('node-excel-export');



const styles = {
    headerDark: {
        font: {
            color: {
                rgb: 'FF000000'
            },
            sz: 14
        }
    }
};

const specification = {
    dishname: {
        displayName: 'Dishname',
        headerStyle: styles.headerDark,
        width: 150
    },
    quantity: {
        displayName: 'Quantity',
        headerStyle: styles.headerDark,
        width: 150
    },
    createdtillnow: {
        displayName: 'Created-tillnow',
        headerStyle: styles.headerDark,
        width: 220
    },
    prediction: {
        displayName: 'Prediction',
        headerStyle: styles.headerDark,
        width: 220
    }
};


//donload report
router.get('/download-report', function(req, res, next) {
    addProduct.find({}, function(er, ress) {
        console.log(ress);
        if (er) {
            console.log("ee");
            return next(er);
        } else {
            const report = excel.buildExport(
                [{
                    name: 'Kitchen display report',
                    specification: specification, // <- Report specification 
                    data: ress
                }]
            );
            res.attachment('kitchen_Display_report.xlsx');
            return res.send(report);
            //return res.json(result);
        }



    });
});
/*router.get('/', function(req, res, next) {
    res.send('respond with a resource');
});
*/

//place order
router.post('/placeorder', function(req, res, next) {
    var addProductData = new addProduct();
    addProductData.dishname = req.body.dishname;
    addProductData.quantity = req.body.quantity;
    addProductData.save(function(er) {
        if (er) {
            console.log("ee");
            return next(er);
        }
        res.send("done");
    });

});


//get details
router.get('/orderdetails', function(req, res, next) {
    addProduct.find({}, function(er, ress) {
        if (er) {
            console.log("ee");
            return next(er);
        }
        res.send(ress);
    });

});

//set prediction value
router.post('/setPrediction', function(req, res, next) {
    var addProductData = new addProduct();
    const userUpdate = { "prediction": req.body.prediction };
    var query = { "dishname": req.body.dishname };
    // console.log(query);
    addProductData.dishname = req.body.dishname;
    addProductData.prediction = req.body.prediction;
    addProduct.findOneAndUpdate(query, userUpdate, { upsert: true, new: true }, function(err, doc) {
        if (err) return res.send(500, { error: err });
        return res.send(doc);
    });

});



module.exports = router;